package com.takusemba.mapme.widgets;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.takusemba.mapme.listeners.OnKeyboardClosedListener;

/**
 * Created by takusemba on 15/11/23.
 */
public class CustomCoordinatorLayout extends CoordinatorLayout {

	OnKeyboardClosedListener onKeyboardClosedListener = null;


	public CustomCoordinatorLayout(Context context) {
		super(context);
	}

	public CustomCoordinatorLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomCoordinatorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public void setOnKeyboardClosedListener(OnKeyboardClosedListener listener) {
		onKeyboardClosedListener = listener;
	}

	@Override
	public boolean dispatchKeyEventPreIme(KeyEvent event) {
		if (event != null) {
			int keyCode = event.getKeyCode();
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if(onKeyboardClosedListener != null) {
					onKeyboardClosedListener.keyboardClosed();
				}
			}
		}
		return super.dispatchKeyEventPreIme(event);
	}


}
