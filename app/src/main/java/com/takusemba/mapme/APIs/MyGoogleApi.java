package com.takusemba.mapme.APIs;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.takusemba.mapme.activities.MainActivity;
import com.takusemba.mapme.listeners.OnDistanceListener;
import com.takusemba.mapme.models.CurrentLocation;
import com.takusemba.mapme.models.OrmaDatabase;
import com.takusemba.mapme.utils.Map;
import com.takusemba.mapme.utils.OrmaUtil;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by takusemba on 15/11/24.
 */
public class MyGoogleApi
		implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener,
		LocationListener {

	private static final String TAG = "MyGoogleApi";

	private Context mContext;
	private TextView mTextView;
	private Location mLocation;
	private AVLoadingIndicatorView mIndicator;

	private GoogleApiClient mGoogleApiClient;

	public MyGoogleApi(Context context) {
		this.mContext = context;
		mGoogleApiClient = new GoogleApiClient.Builder(mContext, this, this)
				.addApi(LocationServices.API)
				.build();
	}

	public MyGoogleApi(MainActivity activity, TextView textView, AVLoadingIndicatorView indicatorView) {
		mContext = activity;
		mTextView = textView;
		mIndicator = indicatorView;
		mIndicator.setVisibility(View.VISIBLE);
		textView.setVisibility(View.GONE);
		mGoogleApiClient = new GoogleApiClient.Builder(mContext, this, this)
				.addApi(LocationServices.API)
				.build();
		activity.setOnDistanceListener(new OnDistanceListener() {
			@Override
			public void distanceChanged() {
				if (mLocation != null && mTextView != null) {
					Map.getClosestDistance(mLocation, mTextView, mIndicator, mContext);
				}
			}
		});

	}

	public void onStart() {
		if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
			mGoogleApiClient.connect();
		}
	}

	public boolean isConnected() {
		return mGoogleApiClient.isConnected();
	}

	public void disconnect() {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onConnected(Bundle bundle) {
		Log.d(TAG, "onConnected is called");
		LocationRequest locationRequest = LocationRequest.create()
				.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
				.setFastestInterval(5000)
				.setInterval(10000);
		if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
		}
	}

	@Override
	public void onConnectionSuspended(int i) {
		Log.i(TAG, "Connection suspended");
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
	}

	@Override
	public void onLocationChanged(Location location) {
		mLocation = location;
		OrmaDatabase orma = OrmaDatabase.builder(mContext).build();
		if (OrmaUtil.isCurrentLocationEmpty(orma)) {
			CurrentLocation currentLocation = new CurrentLocation(location.getLatitude(), location.getLongitude());
			orma.insertIntoCurrentLocation(currentLocation);
		} else {
			orma.updateCurrentLocation().idEq(0).latitude(location.getLatitude()).longitude(location.getLongitude()).execute();
		}
		if (mContext instanceof MainActivity) {
			Map.getClosestDistance(location, mTextView, mIndicator, mContext);
		}
	}
}
