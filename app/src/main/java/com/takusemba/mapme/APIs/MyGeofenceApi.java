package com.takusemba.mapme.APIs;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.receivers.AlarmLocationReceiver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by takusemba on 15/11/25.
 */
public class MyGeofenceApi implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

	private final String TAG = getClass().getSimpleName();

	private Context mContext;
	private double mLatitude;
	private double mLongitude;
	private double mRadius;
	private long mId;

	private GoogleApiClient mGoogleApiClient;

	OnApiPreparedListener onApiPreparedListener;

	public void setOnApiPreparedLister(OnApiPreparedListener listener) {
		onApiPreparedListener = listener;
	}

	public MyGeofenceApi(Context context, double latitude, double longitude, double radius, long id) {
		this.mContext = context;
		this.mLatitude = latitude;
		this.mLongitude = longitude;
		this.mRadius = radius;
		this.mId = id;
		mGoogleApiClient = new GoogleApiClient.Builder(mContext, this, this)
				.addApi(LocationServices.API)
				.build();
	}

	public MyGeofenceApi(Context context, long id) {
		mContext = context;
		mId = id;
		mGoogleApiClient = new GoogleApiClient.Builder(mContext, this, this)
				.addApi(LocationServices.API)
				.build();
	}

	public void onStart() {
		mGoogleApiClient.connect();
	}

	public void disconnect() {
		mGoogleApiClient.disconnect();
	}

	public boolean isConnected() {
		return mGoogleApiClient.isConnected();
	}


	@Override
	public void onConnected(Bundle bundle) {
		onApiPreparedListener.ApiPrepared();
	}

	public void unRegisterGeofence() {
		List<String> ids = new ArrayList<>();
		ids.add(String.valueOf(mId));
		PendingResult<Status> pendingResult = LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, ids);
		pendingResult.setResultCallback(new ResultCallback<Status>() {
			@Override
			public void onResult(Status status) {
				if (status.isSuccess()) {
					Log.d(TAG, "unregistered GeofenceAPI");
				} else {
					Log.d(TAG, "failed unregistering GeofenceAPI");
				}
			}
		});
		ids.clear();
	}

	public void registerGeofence() {
		Log.d(TAG, "registered GeofenceAPI : " + mLatitude + " Longitude : " + mLongitude + "  Radius : " + mRadius);
		Geofence geofence = new Geofence.Builder()
				.setRequestId(String.valueOf(mId))
				.setCircularRegion(mLatitude, mLongitude, (float) mRadius)
				.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL)
				.setLoiteringDelay(1000)
				.setExpirationDuration(Geofence.NEVER_EXPIRE)
				.build();
		Intent intent = new Intent(mContext, AlarmLocationReceiver.class);
		Bundle bundle = new Bundle();
		bundle.putLong(Constants.BundleKey.ALARM_LOCATION_ID, mId);
		intent.putExtras(bundle);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
			builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
			builder.addGeofence(geofence);
			GeofencingRequest GeofenceRequest = builder.build();
			LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, GeofenceRequest, pendingIntent);
		}
	}

	public void reRegisterGeofence() {
		List<String> ids = new ArrayList<>();
		ids.add(String.valueOf(mId));
		PendingResult<Status> pendingResult = LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, ids);
		pendingResult.setResultCallback(new ResultCallback<Status>() {
			@Override
			public void onResult(Status status) {
				if (status.isSuccess()) {
					Log.d(TAG, "unregistered GeofenceAPI");
					Geofence geofence = new Geofence.Builder()
							.setRequestId(String.valueOf(mId))
							.setCircularRegion(mLatitude, mLongitude, (float) mRadius)
							.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_DWELL)
							.setLoiteringDelay(1000)
							.setExpirationDuration(Geofence.NEVER_EXPIRE)
							.build();
					Intent intent = new Intent(mContext, AlarmLocationReceiver.class);
					Bundle bundle = new Bundle();
					bundle.putLong(Constants.BundleKey.ALARM_LOCATION_ID, mId);
					intent.putExtras(bundle);
					PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
					if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
						GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
						builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
						builder.addGeofence(geofence);
						GeofencingRequest GeofenceRequest = builder.build();
						LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, GeofenceRequest, pendingIntent);
					}
				} else {
					Log.d(TAG, "failed unregistering GeofenceAPI");
				}
			}
		});

	}

	@Override
	public void onConnectionSuspended(int i) {
		Log.i(TAG, "Connection suspended");
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());
	}
}
