package com.takusemba.mapme.models;

import com.github.gfx.android.orma.annotation.Column;
import com.github.gfx.android.orma.annotation.PrimaryKey;
import com.github.gfx.android.orma.annotation.Table;

/**
 * Created by takusemba on 15/11/27.
 */
@Table
public class AutoQuery {

	@PrimaryKey
	public long id;

	@Column
	public String query;

	public AutoQuery() {
	}

	public AutoQuery(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
}
