package com.takusemba.mapme.models;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;

import com.takusemba.mapme.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by takusemba on 15/11/30.
 */
public class NavItem {

	private String action;
	private Drawable icon;

	public NavItem(){
	}

	public NavItem(String action, Drawable drawable){
		this.action = action;
		this.icon = drawable;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Drawable getIcon() {
		return icon;
	}

	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	public List<NavItem> setNavitems(Context context){
		List<String> actions = Arrays.asList(context.getResources().getStringArray(R.array.drawer_actions));
		TypedArray images = context.getResources().obtainTypedArray(R.array.drawer_icons);
		List<Drawable> icons = new ArrayList<>();
		for (int i=0; i<images.length(); i++) {
			icons.add(images.getDrawable(i));
		}
		List<NavItem> navItems = new ArrayList<>();
		for (int i=0; i<actions.size(); i++){
			NavItem navItem = new NavItem(actions.get(i), icons.get(i));
			navItems.add(navItem);
		}
		return navItems;
	}
}
