package com.takusemba.mapme.models;


import com.github.gfx.android.orma.annotation.Column;
import com.github.gfx.android.orma.annotation.PrimaryKey;
import com.github.gfx.android.orma.annotation.Table;

/**
 * Created by takusemba on 15/12/07.
 */
@Table
public class CurrentLocation {

	@PrimaryKey
	public long id;

	@Column
	public double latitude;

	@Column
	public double longitude;

	public CurrentLocation() {

	}

	public CurrentLocation(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
}
