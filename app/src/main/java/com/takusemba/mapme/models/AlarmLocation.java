package com.takusemba.mapme.models;


import com.github.gfx.android.orma.annotation.Column;
import com.github.gfx.android.orma.annotation.PrimaryKey;
import com.github.gfx.android.orma.annotation.Table;

import java.io.Serializable;

/**
 * Created by takusemba on 15/11/23.
 */
@Table
public class AlarmLocation implements Serializable {

	@PrimaryKey
	public long id;

	@Column
	public String name;

	@Column
	public double latitude;

	@Column
	public double longitude;

	@Column
	public int radius;

	@Column
	public boolean checked;


	public AlarmLocation() {
	}

	public AlarmLocation(String name, double latitude, double longitude, int radius) {
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.checked = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
}
