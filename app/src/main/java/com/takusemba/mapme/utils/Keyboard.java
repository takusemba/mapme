package com.takusemba.mapme.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by takusemba on 15/11/23.
 */
public class Keyboard {

	public static void hideKeyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
	}
}
