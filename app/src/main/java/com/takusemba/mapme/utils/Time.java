package com.takusemba.mapme.utils;

import android.content.Context;
import android.widget.TextView;

import com.takusemba.mapme.R;

import java.util.Calendar;

/**
 * Created by takusemba on 15/12/02.
 */
public class Time {

	public static void setCurrentTime(Context context, TextView textView){
		Calendar c = Calendar.getInstance();
		int min = c.get(Calendar.MINUTE);
		int hour = c.get(Calendar.HOUR_OF_DAY);
		textView.setText(context.getString(R.string.current_time, String.format("%02d", hour), String.format("%02d", min)));
	}
}
