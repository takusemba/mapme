package com.takusemba.mapme.utils;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.takusemba.mapme.R;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.OrmaDatabase;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by takusemba on 15/11/25.
 */
public class Map {

	private final String TAG = getClass().getSimpleName();

	private static String addressToText(Address address) {
		//skip country name
		final StringBuilder addressText = new StringBuilder();
		for (int i = 1, max = address.getMaxAddressLineIndex(); i <= max; i++) {
			addressText.append(address.getAddressLine(i));
		}
		return addressText.toString().replace("〒"+address.getPostalCode(), "").trim();
	}

	public static String getTitle(Activity activity, LatLng latLng){
		String title = "";
		try {
			Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
			List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
			if (addresses != null && addresses.size() > 0) {
				title = Map.addressToText(addresses.get(0));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return title;
	}

	public static LatLng getLatLng(Activity activity, String query){
		Geocoder geocoder = new Geocoder(activity);
		List<Address> addresses = null;
		try {
			addresses = geocoder.getFromLocationName(query, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (!Map.isAddressesEmpty(addresses)) {
			return new LatLng(addresses.get(0).getLatitude(), addresses.get(0).getLongitude());
		}
		return null;
	}

	public static CircleOptions getCircle(Activity activity, LatLng latLng, SeekBar seekBar){
		return new CircleOptions()
				.center(latLng)
				.radius(seekBar.getProgress())
				.strokeWidth(0)
				.fillColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.colorAccentWithAlpha));
	}

	public static CircleOptions getCircle(Activity activity, LatLng latLng, AlarmLocation alarmLocation){
		return new CircleOptions()
				.center(latLng)
				.radius(alarmLocation.getRadius())
				.strokeWidth(0)
				.fillColor(ContextCompat.getColor(activity.getApplicationContext(), R.color.colorAccentWithAlpha));
	}


	public static boolean isAddressesEmpty(List<Address> addresses){
		return addresses == null ||  addresses.size() == 0;
	}

	public static void getClosestDistance(Location location, TextView textView, AVLoadingIndicatorView indicatorView, Context context){
		boolean isFirst = true;
		float closestDistance = 0;
		float distance;
		indicatorView.setVisibility(View.GONE);
		textView.setVisibility(View.VISIBLE);
		OrmaDatabase orma = OrmaDatabase.builder(context).build();
		List<AlarmLocation> alarmLocations = orma.selectFromAlarmLocation().toList();
		for (AlarmLocation alarmLocation : alarmLocations){
			if(alarmLocation.isChecked()){
				distance = getDistance(alarmLocation, location);
				if (distance <= 0){
					textView.setText(R.string.within_alarm);
					return;
				} else {
					if(isFirst || closestDistance > distance) {
						closestDistance = distance;
						isFirst = false;
					}
				}
			}
		}
		if(closestDistance == 0){
			textView.setText(R.string.no_alarm_set);
		} else {
			textView.setText(context.getString(R.string.distance_to_alarm, String.format("%.2f", closestDistance / 1000.0)));
		}
	}

	public static float getDistance(AlarmLocation alarmLocation, Location location){
		LatLng latLng = new LatLng(alarmLocation.getLatitude(), alarmLocation.getLongitude());
		return getDistanceToCenter(latLng, location) - (float) alarmLocation.getRadius();
	}


	public static float getDistanceToCenter(LatLng latLng, Location location){
		Location location1 = getLocation(latLng);
		return location1.distanceTo(location);
	}

	private static Location getLocation(LatLng latLng){
		Location location = new Location("");
		location.setLatitude(latLng.latitude);
		location.setLongitude(latLng.longitude);
		return location;
	}
}
