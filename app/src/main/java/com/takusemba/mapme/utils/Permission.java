package com.takusemba.mapme.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

/**
 * Created by takusemba on 15/11/22.
 */

/**
 * Utility class for access to runtime permissions.
 */
public class Permission {

	@TargetApi(Build.VERSION_CODES.M)
	public static boolean isPermissionGranted(Activity activity) {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

	}

	@TargetApi(Build.VERSION_CODES.M)
	public static boolean isPermissionDenied(Activity activity) {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED;
	}

	public static boolean isCurrentLocationGranted(Activity activity) {
		LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isCurrentLocationDenied(Activity activity) {
		LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		return !manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}


	public static void requestPermission(Activity activity, int requestId, String permission) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			ActivityCompat.requestPermissions(activity, new String[]{permission}, requestId);
		}
	}
}

