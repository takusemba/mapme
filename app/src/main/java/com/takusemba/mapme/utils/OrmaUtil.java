package com.takusemba.mapme.utils;

import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.AutoQuery;
import com.takusemba.mapme.models.OrmaDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by takusemba on 15/11/26.
 */
public class OrmaUtil {

	public static boolean isAlarmLocationsEmpty(List<AlarmLocation> alarmLocations) {
		return alarmLocations == null || alarmLocations.size() == 0;
	}

	public static String[] getSuggestions(OrmaDatabase orma) {
		List<AutoQuery> autoQueries = orma.selectFromAutoQuery().toList();
		List<String> queriesList = new ArrayList<String>();
		for (AutoQuery autoQuery : autoQueries) {
			if (Collections.frequency(queriesList, autoQuery.getQuery()) == 0) {
				queriesList.add(autoQuery.getQuery());
			}
		}
		String[] suggestions = new String[queriesList.size()];
		return queriesList.toArray(suggestions);
	}

	public static boolean isCurrentLocationEmpty(OrmaDatabase orma) {
		return orma.selectFromCurrentLocation().count() == 0;
	}
}
