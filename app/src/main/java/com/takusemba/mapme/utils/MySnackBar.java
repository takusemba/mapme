package com.takusemba.mapme.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.R;
import com.takusemba.mapme.activities.MapBaseActivity;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.OrmaDatabase;

import java.util.List;

/**
 * Created by takusemba on 15/11/26.
 */
public class MySnackBar {

	private static Snackbar mNetworkSnackBar;
	private static Snackbar mPermissionSnackBar;
	private static Snackbar mCurrentLocationSnackBar;

	private static boolean isPermissionSnackBarClicked = false;
	private static boolean isCurrentLocationSnackBarClicked = false;


	public static boolean isShown() {
		return (mNetworkSnackBar != null && mNetworkSnackBar.isShown()) || (mPermissionSnackBar != null && mPermissionSnackBar.isShown() || (mCurrentLocationSnackBar != null && mCurrentLocationSnackBar.isShown()));
	}

	public static void dismiss() {
		if (mNetworkSnackBar != null && mNetworkSnackBar.isShown()) {
			mNetworkSnackBar.dismiss();
		} else if (mPermissionSnackBar != null && mPermissionSnackBar.isShown()) {
			mPermissionSnackBar.dismiss();
		} else if (mCurrentLocationSnackBar != null && mCurrentLocationSnackBar.isShown()) {
			mCurrentLocationSnackBar.dismiss();
		}
	}

	public static void dismissNetworkSnackBar() {
		if (mNetworkSnackBar != null && mNetworkSnackBar.isShown()) {
			mNetworkSnackBar.dismiss();
		}
	}

	public static void dismissPermissionSnackBar() {
		if (mPermissionSnackBar != null && mPermissionSnackBar.isShown()) {
			mPermissionSnackBar.dismiss();
		}
	}

	public static void dismissCurrentLocationSnackBar() {
		if (mCurrentLocationSnackBar != null && mCurrentLocationSnackBar.isShown()) {
			mCurrentLocationSnackBar.dismiss();
		}
	}

	public static void showNetworkSnackBar(View view) {
		mNetworkSnackBar = Snackbar.make(view, R.string.snackbar_no_internet, Snackbar.LENGTH_INDEFINITE);
		mNetworkSnackBar.show();
	}

	@TargetApi(Build.VERSION_CODES.M)
	public static void showPermissionSnackBar(final Activity activity, View view) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			return;
		}
		if (!activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
			mPermissionSnackBar = Snackbar.make(view, R.string.snackbar_no_permission_dialog, Snackbar.LENGTH_INDEFINITE);
			mPermissionSnackBar.show();
		} else {
			mPermissionSnackBar = Snackbar.make(view, R.string.snackbar_no_permission, Snackbar.LENGTH_INDEFINITE).setAction(R.string.snackbar_settings, new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					isPermissionSnackBarClicked = true;
				}
			}).setCallback(new Snackbar.Callback() {
				@Override
				public void onDismissed(Snackbar snackbar, int event) {
					super.onDismissed(snackbar, event);
					if (isPermissionSnackBarClicked) {
						Permission.requestPermission(activity, Constants.PermissionCode.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
						((MapBaseActivity) activity).setPermissionDialogShown(true);
						isPermissionSnackBarClicked = false;
					}

				}
			});
			mPermissionSnackBar.show();
		}
	}

	public static void showCurrentLocationSnackBar(final Activity activity, View view) {
		mCurrentLocationSnackBar = Snackbar.make(view, R.string.snackbar_no_current_location, Snackbar.LENGTH_INDEFINITE).setAction(R.string.snackbar_settings, new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				isCurrentLocationSnackBarClicked = true;
			}
		}).setCallback(new Snackbar.Callback() {
			@Override
			public void onDismissed(Snackbar snackbar, int event) {
				super.onDismissed(snackbar, event);
				if (isCurrentLocationSnackBarClicked) {
					activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
					isCurrentLocationSnackBarClicked = false;
				}
			}
		});
		mCurrentLocationSnackBar.show();
	}

	public static void showRadiusSnackBar(View view) {
		Snackbar.make(view, R.string.radius_snack, Snackbar.LENGTH_LONG).show();
	}

	public enum SettingAction {
		CACHE,
		ALARMS,
		QUERIES
	}

	public static void showSettingsSnackBar(final Activity activity, SettingAction type, View view, final OrmaDatabase orma) {
		Snackbar snackbar = null;
		View.OnClickListener listener = null;
		switch (type) {
			case CACHE:
				snackbar = Snackbar.make(view, R.string.settings_snack_cache_before, Snackbar.LENGTH_LONG);
				listener = new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						deleteAllGeofence(activity);
						orma.deleteFromAlarmLocation().execute();
						orma.deleteFromAutoQuery().execute();
						Snackbar.make(view, R.string.settings_snack_cache_after, Snackbar.LENGTH_LONG).show();
					}
				};
				break;
			case ALARMS:
				snackbar = Snackbar.make(view, R.string.settings_snack_alarms_before, Snackbar.LENGTH_LONG);
				listener = new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						deleteAllGeofence(activity);
						orma.deleteFromAlarmLocation().execute();
						Snackbar.make(view, R.string.settings_snack_alarms_after, Snackbar.LENGTH_LONG).show();
					}
				};
				break;
			case QUERIES:
				snackbar = Snackbar.make(view, R.string.settings_snack_queries_before, Snackbar.LENGTH_LONG);
				listener = new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						orma.deleteFromAutoQuery().execute();
						Snackbar.make(view, R.string.settings_snack_queries_after, Snackbar.LENGTH_LONG).show();
					}
				};
				break;
		}
		snackbar.setAction(R.string.ok, listener).show();
	}

	private static void deleteAllGeofence(Activity activity) {
		OrmaDatabase orma = OrmaDatabase.builder(activity).build();
		List<AlarmLocation> alarmLocations = orma.selectFromAlarmLocation().toList();
		for (AlarmLocation alarmLocation : alarmLocations) {
			final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(activity, alarmLocation.id);
			myGeofenceApi.onStart();
			myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
				@Override
				public void ApiPrepared() {
					myGeofenceApi.unRegisterGeofence();
				}
			});
		}
	}
}
