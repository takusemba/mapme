package com.takusemba.mapme.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by takusemba on 15/11/26.
 */
public class ConnectionReceiver extends BroadcastReceiver {

	private Observer mObserver;

	public ConnectionReceiver(Observer observer) {
		mObserver = observer;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		mObserver.onConnectionStatusChange();
	}

	public interface Observer {
		void onConnectionStatusChange();
	}
}
