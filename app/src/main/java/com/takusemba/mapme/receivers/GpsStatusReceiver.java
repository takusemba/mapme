package com.takusemba.mapme.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by takusemba on 15/12/06.
 */
public class GpsStatusReceiver extends BroadcastReceiver {

	private Observer mObserver;

	public GpsStatusReceiver(Observer observer) {
		mObserver = observer;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		mObserver.onGpsStatusChange();
	}

	public interface Observer {
		void onGpsStatusChange();
	}
}
