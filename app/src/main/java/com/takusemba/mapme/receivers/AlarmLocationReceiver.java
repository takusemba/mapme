package com.takusemba.mapme.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.GeofencingEvent;
import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.activities.PopUpActivity;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.OrmaDatabase;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by takusemba on 15/11/24.
 */
public class AlarmLocationReceiver extends BroadcastReceiver {

	protected final String TAG = getClass().getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		long id = intent.getExtras().getLong(Constants.BundleKey.ALARM_LOCATION_ID);
		GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
		Log.d(TAG, "entered alarm range " + geofencingEvent.getGeofenceTransition() + "  " + DateFormat.getTimeInstance().format(new Date()));

		OrmaDatabase orma = OrmaDatabase.builder(context).build();
		AlarmLocation alarmLocation = orma.selectFromAlarmLocation().idEq(id).get(0);
		orma.updateAlarmLocation().idEq(id).checked(false).execute();

		final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(context, alarmLocation.id);
		myGeofenceApi.onStart();
		myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
			@Override
			public void ApiPrepared() {
				myGeofenceApi.unRegisterGeofence();
			}
		});

		Intent i = new Intent(context, PopUpActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(i);
	}

}
