package com.takusemba.mapme.listeners;

import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by takusemba on 15/12/07.
 */
public interface OnHeaderBoundListener {
	void OnHeaderBound(TextView headerText, AVLoadingIndicatorView indicator);
}
