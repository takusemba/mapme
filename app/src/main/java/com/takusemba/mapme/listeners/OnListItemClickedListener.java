package com.takusemba.mapme.listeners;

/**
 * Created by takusemba on 15/12/07.
 */
public interface OnListItemClickedListener {
	void onListItemClicked(int position);
}
