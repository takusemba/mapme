package com.takusemba.mapme.listeners;

/**
 * Created by takusemba on 15/11/23.
 */
public interface OnKeyboardClosedListener {

	void keyboardClosed();
}
