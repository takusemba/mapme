package com.takusemba.mapme;

/**
 * Created by takusemba on 15/11/26.
 */
public class Constants {

	public class RequestCode{
		public static final int MapActivity = 1;
		public static final int DetailActivity = 2;
	}

	public class PermissionCode{
		public final static int ACCESS_FINE_LOCATION = 0;
	}

	public class BundleKey{
		public final static String POSITION = "position";
		public final static String ALARM_LOCATION = "alarm_location";
		public final static String ALARM_LOCATION_ID = "alarm_location_id";
	}

	public class TransitionKey{
		public final static String FAB = "fab";
	}
}
