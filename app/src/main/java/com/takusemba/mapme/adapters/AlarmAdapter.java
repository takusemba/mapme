package com.takusemba.mapme.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rey.material.widget.Switch;
import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.R;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.listeners.OnDistanceListener;
import com.takusemba.mapme.listeners.OnHeaderBoundListener;
import com.takusemba.mapme.listeners.OnListItemClickedListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.OrmaDatabase;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

/**
 * Created by takusemba on 15/11/23.
 */
public class AlarmAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private List<AlarmLocation> mAlarmLocations;
	private Context mContext;
	private OnDistanceListener onDistanceListener;
	private OnListItemClickedListener onListItemClickedListener;
	private OnHeaderBoundListener onHeaderBoundListener;
	private OrmaDatabase mOrma;

	public static final int TYPE_HEADER = 111;
	public static final int TYPE_FOOTER = 222;
	public static final int TYPE_ITEM = 333;

	public AlarmAdapter(Context context, List<AlarmLocation> AlarmLocations, OnListItemClickedListener listener1, OnHeaderBoundListener listener2) {
		super();
		mContext = context;
		mAlarmLocations = AlarmLocations;
		onListItemClickedListener = listener1;
		onHeaderBoundListener = listener2;
		mOrma = OrmaDatabase.builder(mContext).build();
	}

	public void bindListener(OnDistanceListener listener3) {
		this.onDistanceListener = listener3;
	}

	public void setDataAndNotify() {
		this.mAlarmLocations = mOrma.selectFromAlarmLocation().toList();
		notifyDataSetChanged();
	}

	public boolean isEmpty() {
		return mAlarmLocations == null || mAlarmLocations.isEmpty() || mAlarmLocations.size() == 0;
	}

	class FooterViewHolder extends RecyclerView.ViewHolder {

		public FooterViewHolder(View itemView) {
			super(itemView);
		}
	}

	public class HeaderViewHolder extends RecyclerView.ViewHolder {

		AVLoadingIndicatorView indicator;

		TextView headerText;

		public HeaderViewHolder(View itemView) {
			super(itemView);
			this.headerText = (TextView) itemView.findViewById(R.id.header_text);
			this.indicator = (AVLoadingIndicatorView) itemView.findViewById(R.id.indicator);
		}
	}

	public class NormalViewHolder extends RecyclerView.ViewHolder {
		private TextView name;
		private TextView radius;
		private Switch switchBar;
		private RelativeLayout mLayout;

		public NormalViewHolder(View view) {
			super(view);
			name = (TextView) view.findViewById(R.id.name);
			radius = (TextView) view.findViewById(R.id.radius);
			switchBar = (Switch) view.findViewById(R.id.switch_bar);
			mLayout = (RelativeLayout) view.findViewById(R.id.list_item_container);
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v;
		if (viewType == TYPE_FOOTER) {
			v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_footer, parent, false);
			return new FooterViewHolder(v);
		} else if (viewType == TYPE_HEADER) {
			v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_header, parent, false);
			return new HeaderViewHolder(v);
		} else {
			v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_swipe_list_view, parent, false);
			return new NormalViewHolder(v);
		}
	}

	@Override
	public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
		if (holder instanceof HeaderViewHolder) {
			HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
			onHeaderBoundListener.OnHeaderBound(headerHolder.headerText, headerHolder.indicator);
		} else if (holder instanceof FooterViewHolder) {
		} else if (holder instanceof NormalViewHolder) {
			final NormalViewHolder normalViewholder = (NormalViewHolder) holder;
			final AlarmLocation alarmLocation = mAlarmLocations.get(position - 1);
			normalViewholder.name.setText(alarmLocation.getName());
			normalViewholder.radius.setText(mContext.getString((R.string.radius), String.format("%.2f", alarmLocation.getRadius() / 1000.0)));
			normalViewholder.switchBar.setChecked(alarmLocation.isChecked());
			normalViewholder.switchBar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					normalViewholder.switchBar.setEnabled(false);
					final boolean checked = normalViewholder.switchBar.isChecked();
					if (!checked) {
						final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(mContext, alarmLocation.id);
						myGeofenceApi.onStart();
						myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
							@Override
							public void ApiPrepared() {
								myGeofenceApi.unRegisterGeofence();
								normalViewholder.switchBar.setChecked(checked);
								mOrma.updateAlarmLocation().idEq(alarmLocation.id).checked(checked).execute();
								onDistanceListener.distanceChanged();
							}
						});
					} else {
						final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(mContext, alarmLocation.getLatitude(), alarmLocation.getLongitude(), alarmLocation.getRadius(), alarmLocation.id);
						myGeofenceApi.onStart();
						myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
							@Override
							public void ApiPrepared() {
								myGeofenceApi.registerGeofence();
								normalViewholder.switchBar.setChecked(checked);
								mOrma.updateAlarmLocation().idEq(alarmLocation.id).checked(checked).execute();
								onDistanceListener.distanceChanged();
							}
						});
					}
					normalViewholder.switchBar.setEnabled(true);
				}
			});
			normalViewholder.mLayout.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					onListItemClickedListener.onListItemClicked(position);
				}
			});
		}
	}

	@Override
	public int getItemViewType(int position) {
		if (isPositionHeader(position)) {
			return TYPE_HEADER;
		} else if (isPositionFooter(position)) {
			return TYPE_FOOTER;
		} else {
			return TYPE_ITEM;
		}
	}

	private boolean isPositionHeader(int position) {
		return position == 0;
	}

	private boolean isPositionFooter(int position) {
		return position == mAlarmLocations.size() + 1;
	}

	@Override
	public int getItemCount() {
		return mAlarmLocations.size() + 2;
	}

	public AlarmLocation getItem(int position) {
		return mAlarmLocations.get(position);
	}
}
