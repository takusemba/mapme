package com.takusemba.mapme.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.takusemba.mapme.R;
import com.takusemba.mapme.models.NavItem;

import java.util.List;

/**
 * Created by takusemba on 15/11/30.
 */
public class NavAdapter extends ArrayAdapter<NavItem> {

	private List<NavItem> mNacItems;
	private Context mContext;

	public NavAdapter(Context context, int resource, List<NavItem> navItems) {
		super(context, resource, navItems);
		mContext = context;
		mNacItems = navItems;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = View.inflate(getContext(), R.layout.item_nav_list_view, null);
			holder = new ViewHolder(convertView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.actionText.setText(mNacItems.get(position).getAction());
		holder.actionIcon.setImageDrawable(mNacItems.get(position).getIcon());

		return convertView;
	}

	private static class ViewHolder {

		private TextView actionText;

		private ImageView actionIcon;

		private ViewHolder(View view) {
			actionText = (TextView) view.findViewById(R.id.nav_text);
			actionIcon = (ImageView) view.findViewById(R.id.nav_image);
		}
	}
}
