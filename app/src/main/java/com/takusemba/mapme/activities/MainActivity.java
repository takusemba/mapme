package com.takusemba.mapme.activities;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.APIs.MyGoogleApi;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.R;
import com.takusemba.mapme.adapters.AlarmAdapter;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.listeners.OnDistanceListener;
import com.takusemba.mapme.listeners.OnHeaderBoundListener;
import com.takusemba.mapme.listeners.OnListItemClickedListener;
import com.takusemba.mapme.listeners.OnSwipeableRecyclerViewTouchListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.utils.MySnackBar;
import com.takusemba.mapme.utils.Permission;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends MapBaseActivity
		implements FloatingActionButton.OnClickListener,
		OnListItemClickedListener,
		OnHeaderBoundListener {

	@Bind(R.id.toolbar)
	Toolbar mToolbar;
	@Bind(R.id.recycler_view)
	RecyclerView mRecyclerView;
	@Bind(R.id.fab)
	FloatingActionButton mFab;
	private MyGoogleApi mMyGoogleApi;
	private AlarmAdapter mAlarmAdapter;
	private OnListItemClickedListener onListItemClickedListener;
	private OnHeaderBoundListener onHeaderBoundListener;
	private OnSwipeableRecyclerViewTouchListener onSwipeableRecyclerViewTouchListener;
	private OnDistanceListener onDistanceListener;

	private void setOnListItemClickedListener(OnListItemClickedListener listener) {
		onListItemClickedListener = listener;
	}

	private void setOnHeaderBoundListener(OnHeaderBoundListener listener) {
		onHeaderBoundListener = listener;
	}

	public void setOnDistanceListener(OnDistanceListener listener) {
		onDistanceListener = listener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_main);
		}
		setDrawerAndAd();
		setOnListItemClickedListener(this);
		setOnHeaderBoundListener(this);
		SetOnSwipeableRecyclerViewTouchListener();
		mCoordinatorLayout = ButterKnife.findById(this, R.id.coordinator_layout);
		mFab.setOnClickListener(this);
		List<AlarmLocation> alarmLocations = mOrma.selectFromAlarmLocation().toList();
		mAlarmAdapter = new AlarmAdapter(this, alarmLocations, onListItemClickedListener, onHeaderBoundListener);
		LinearLayoutManager layoutManager = new LinearLayoutManager(this);
		layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		mRecyclerView.setLayoutManager(layoutManager);
		mRecyclerView.setHasFixedSize(true);
		mRecyclerView.setItemAnimator(new DefaultItemAnimator());
		mRecyclerView.setAdapter(mAlarmAdapter);
		mRecyclerView.addOnItemTouchListener(onSwipeableRecyclerViewTouchListener);
		if (Permission.isPermissionDenied(this)) {
			requestPermission();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		checkState();
		mAlarmAdapter.setDataAndNotify();
		if (onDistanceListener != null) {
			onDistanceListener.distanceChanged();
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mMyGoogleApi != null) {
			mMyGoogleApi.disconnect();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case Constants.RequestCode.MapActivity:
				mAlarmAdapter.setDataAndNotify();
				break;
			case Constants.RequestCode.DetailActivity:
				mAlarmAdapter.setDataAndNotify();
				break;
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case Constants.PermissionCode.ACCESS_FINE_LOCATION:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					mMyGoogleApi.onStart();
				}
		}
	}

	/**
	 * Fab Listener
	 */
	@Override
	public void onClick(View view) {
		if (MySnackBar.isShown()) {
			return;
		}
		Intent intent = new Intent(MainActivity.this, MapsActivity.class);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, mFab, Constants.TransitionKey.FAB);
			startActivityForResult(intent, Constants.RequestCode.MapActivity, options.toBundle());
		} else {
			startActivityForResult(intent, Constants.RequestCode.MapActivity);
		}
	}

	@Override
	public void onListItemClicked(int position) {
		Intent intent = new Intent(MainActivity.this, DetailActivity.class);
		Bundle bundle = new Bundle();
		bundle.putLong(Constants.BundleKey.ALARM_LOCATION_ID, mAlarmAdapter.getItem(position - 1).id);
		intent.putExtras(bundle);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, mFab, Constants.TransitionKey.FAB);
			startActivityForResult(intent, Constants.RequestCode.DetailActivity, options.toBundle());
		} else {
			startActivityForResult(intent, Constants.RequestCode.DetailActivity);
		}
	}

	@Override
	public void OnHeaderBound(TextView headerText, AVLoadingIndicatorView indicator) {
		mMyGoogleApi = new MyGoogleApi(this, headerText, indicator);
		mAlarmAdapter.bindListener(onDistanceListener);
		if (Permission.isPermissionGranted(this)) {
			mMyGoogleApi.onStart();
		}
	}

	private void SetOnSwipeableRecyclerViewTouchListener() {
		onSwipeableRecyclerViewTouchListener = new OnSwipeableRecyclerViewTouchListener(mRecyclerView,
				new OnSwipeableRecyclerViewTouchListener.SwipeListener() {
					@Override
					public boolean canSwipe(int position) {
						return true;
					}

					@Override
					public void onDismissedBySwipeLeft(RecyclerView recyclerView, int[] reverseSortedPositions) {
						for (final int position : reverseSortedPositions) {
							deleteItem(position);
						}
						mAlarmAdapter.setDataAndNotify();
					}

					@Override
					public void onDismissedBySwipeRight(RecyclerView recyclerView, int[] reverseSortedPositions) {
						for (final int position : reverseSortedPositions) {
							deleteItem(position);
						}
						mAlarmAdapter.setDataAndNotify();
					}
				});
	}

	private void deleteItem(int position) {
		//headerがあるためposition-1となる
		AlarmLocation alarmLocation = mAlarmAdapter.getItem(position - 1);
		if (alarmLocation.isChecked()) {
			final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(this, alarmLocation.id);
			myGeofenceApi.onStart();
			myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
				@Override
				public void ApiPrepared() {
					myGeofenceApi.unRegisterGeofence();
				}
			});
		}
		mOrma.deleteFromAlarmLocation().idEq(alarmLocation.id).execute();
	}
}
