package com.takusemba.mapme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.R;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.utils.Keyboard;
import com.takusemba.mapme.utils.Map;
import com.takusemba.mapme.utils.MySnackBar;
import com.takusemba.mapme.utils.Permission;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by takusemba on 15/11/28.
 */
public class DetailActivity extends MapBaseActivity
		implements OnMapReadyCallback,
		GoogleMap.OnMapLongClickListener,
		FloatingActionButton.OnClickListener,
		SeekBar.OnSeekBarChangeListener {

	private GoogleMap mMap;
	private SupportMapFragment mMapFragment;
	private Marker mMarker;
	private Circle mCircle;
	private static final int ZOOM_SCALE = 12;
	private AlarmLocation mAlarmLocation;

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Bind(R.id.fab)
	FloatingActionButton mFab;

	@Bind(R.id.seek_bar)
	SeekBar mSeekBar;

	@Bind(R.id.location_name)
	EditText mLocationName;

	@Bind(R.id.radius)
	TextView mRadius;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_detail);
		}
		setDrawerAndAd();
		mCoordinatorLayout = ButterKnife.findById(this, R.id.coordinator_layout);
		long id = getIntent().getExtras().getLong(Constants.BundleKey.ALARM_LOCATION_ID);
		mAlarmLocation = mOrma.selectFromAlarmLocation().where("id = ?", id).valueOrNull();
		setViews();
		setTransition();
		mFab.setOnClickListener(this);
		mSeekBar.setOnSeekBarChangeListener(this);
		mCoordinatorLayout.setOnClickListener(this);
		mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mMapFragment.getMapAsync(this);
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if (Permission.isPermissionGranted(this)) {
			setMap();
		} else {
			requestPermission();
		}
		checkState();
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case Constants.PermissionCode.ACCESS_FINE_LOCATION:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					setMap();
				}
		}
	}

	/**
	 * GoogleMap Listener
	 */

	@Override
	public void onMapLongClick(LatLng latLng) {
		clear();
		String title = Map.getTitle(this, latLng);
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_SCALE));
		mark(latLng, title);
		mLocationName.setText(Map.getTitle(this, latLng));
	}

	/**
	 * Fab Listener
	 */

	@Override
	public void onClick(View view) {
		if (MySnackBar.isShown()) {
			return;
		}
		if (mSeekBar.getProgress() == 0) {
			MySnackBar.showRadiusSnackBar(mCoordinatorLayout);
			return;
		}
		if (mAlarmLocation != null && !mLocationName.getText().toString().equals("")) {
			mOrma.updateAlarmLocation().idEq(mAlarmLocation.id)
					.name(mLocationName.getText().toString())
					.latitude(mMarker.getPosition().latitude)
					.longitude(mMarker.getPosition().longitude)
					.radius(mSeekBar.getProgress())
					.execute();
			final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(DetailActivity.this, mMarker.getPosition().latitude, mMarker.getPosition().longitude, mSeekBar.getProgress(), mAlarmLocation.id);
			myGeofenceApi.onStart();
			myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
				@Override
				public void ApiPrepared() {
					myGeofenceApi.reRegisterGeofence();
					finish();
				}
			});
		} else {
			Snackbar.make(mCoordinatorLayout, R.string.snackbar_no_location_name, Snackbar.LENGTH_LONG).show();
		}
	}

	/**
	 * Seek Bar Listener
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
		if (mCircle != null && mRadius != null) {
			mCircle.setRadius(i);
			mRadius.setText(getString(R.string.radius, String.format("%.2f", mSeekBar.getProgress() / 1000.0)));
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	/**
	 * Other Methods
	 */

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private void setTransition() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mFab.setTransitionName(Constants.TransitionKey.FAB);
		}
	}

	private void setViews() {
		if (mAlarmLocation != null) {
			mSeekBar.setProgress(mAlarmLocation.getRadius());
			mLocationName.setText(mAlarmLocation.getName());
			mRadius.setText(getResources().getText(R.string.radius, String.format("%.2f", mAlarmLocation.getRadius() / 1000.0)));
		}
	}

	private void setMap() {
		if (mMap == null) {
			return;
		}
		if (mAlarmLocation != null) {
			setDefaultMarker();
		}
		mMap.getUiSettings().setMapToolbarEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			mMap.setMyLocationEnabled(true);
		}
		mMap.setOnMapLongClickListener(this);
	}

	private void clear() {
		mMap.clear();
		mMarker = null;
		mCircle = null;
	}

	private void setDefaultMarker() {
		LatLng latLng = new LatLng(mAlarmLocation.getLatitude(), mAlarmLocation.getLongitude());
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_SCALE));
		mark(latLng, mAlarmLocation.getName());
	}

	private void mark(LatLng latLng, String title) {
		mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(title));
		mCircle = mMap.addCircle(Map.getCircle(this, latLng, mSeekBar));
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {

		Keyboard.hideKeyboard(this);
		return super.dispatchTouchEvent(ev);
	}
}
