package com.takusemba.mapme.activities;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.takusemba.mapme.R;
import com.takusemba.mapme.utils.Time;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by takusemba on 15/12/01.
 */
public class AlarmCheckActivity extends BaseActivity
		implements FloatingActionButton.OnClickListener {

	private Handler mHandler = new Handler();
	private final int DURATION = 1000;
	private final int DELAY = 500;
	private boolean isPlaying = false;
	private MediaPlayer mPlayer;
	private AudioManager mAudioManager;
	int userVolume;
	private Vibrator mVibrator;
	long[] pattern = {0, DURATION, DELAY};

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Bind(R.id.fab)
	FloatingActionButton mFab;

	@Bind(R.id.current_time)
	TextView mCurrentTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_check);
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_check_alarm);
		}
		setDrawerAndAd();
		mFab.setOnClickListener(this);
		Time.setCurrentTime(this, mCurrentTime);
		timer();
		setSound();
	}

	@Override
	protected void onResume() {
		super.onResume();
		isPlaying = false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		mPlayer.pause();
		mVibrator.cancel();
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, userVolume, AudioManager.FLAG_PLAY_SOUND);
	}

	public void timer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							Time.setCurrentTime(AlarmCheckActivity.this, mCurrentTime);
							timer();
						}
					});
				} catch (Exception e) {
					Log.d("exception", "exception catched : " + e.getMessage());
				}

			}
		}).start();
	}

	@Override
	public void onClick(View view) {
		if (isPlaying) {
			mFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.button_play));
			mVibrator.cancel();
			mPlayer.pause();
			isPlaying = false;
		} else {
			mFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.button_pause));
			mVibrator.vibrate(pattern, 0);
			mPlayer.seekTo(0);
			mPlayer.start();
			isPlaying = true;
		}
	}

	private void setSound() {
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 2 / 3, AudioManager.FLAG_PLAY_SOUND);
		try {
			mPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
			mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mPlayer.setLooping(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		userVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
		mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
	}
}
