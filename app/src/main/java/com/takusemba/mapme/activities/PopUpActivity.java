package com.takusemba.mapme.activities;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.takusemba.mapme.R;
import com.takusemba.mapme.utils.Time;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by takusemba on 15/12/05.
 */
public class PopUpActivity extends AppCompatActivity
		implements FloatingActionButton.OnClickListener {

	private static final String WAKE_LOCK = "wake_lock";
	private final String TAG = getClass().getSimpleName();

	private Handler mHandler = new Handler();
	private final int DURATION = 1000;
	private final int DELAY = 500;
	private MediaPlayer mPlayer;
	private AudioManager mAudioManager;
	int userVolume;
	private Vibrator mVibrator;
	private AdView mAdView;
	long[] pattern = {0, DURATION, DELAY};

	private PowerManager mPowerManager;
	private PowerManager.WakeLock mWakeLock;

	@Bind(R.id.fab)
	FloatingActionButton mFab;

	@Bind(R.id.current_time)
	TextView mCurrentTime;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_alarm);
		ButterKnife.bind(this);
		getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);

		mPowerManager = (PowerManager) getSystemService(POWER_SERVICE);
		mWakeLock = mPowerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP
				| PowerManager.PARTIAL_WAKE_LOCK
				| PowerManager.ON_AFTER_RELEASE, WAKE_LOCK);
		mWakeLock.acquire(1000);


		mFab.setOnClickListener(this);
		Time.setCurrentTime(this, mCurrentTime);
		timer();
		setSound();

		mAdView = (AdView) findViewById(R.id.ad);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mPlayer.start();
		mVibrator.vibrate(pattern, 0);
	}


	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onClick(View view) {
		mPlayer.pause();
		mVibrator.cancel();
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, userVolume, AudioManager.FLAG_PLAY_SOUND);
		finish();
	}

	public void timer() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							Time.setCurrentTime(PopUpActivity.this, mCurrentTime);
							timer();
						}
					});
				} catch (Exception e) {
					Log.d(TAG, "exception : " + e.getMessage());
				}

			}
		}).start();
	}

	private void setSound() {
		mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) * 2 / 3, AudioManager.FLAG_PLAY_SOUND);
		mPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
		mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mPlayer.setLooping(true);
		userVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
		mVibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
	}
}