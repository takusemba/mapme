package com.takusemba.mapme.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.takusemba.mapme.R;
import com.takusemba.mapme.utils.MySnackBar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by takusemba on 15/12/02.
 */
public class SettingsActivity extends BaseActivity implements TextView.OnClickListener {

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Bind(R.id.review)
	TextView mReviewText;

	@Bind(R.id.cache)
	TextView mCacheText;

	@Bind(R.id.alarms)
	TextView mAlarmsText;

	@Bind(R.id.queries)
	TextView mQueriesText;

	@Bind(R.id.linear_layout_container)
	LinearLayout mLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_settings);
		}
		setDrawerAndAd();
		mReviewText.setOnClickListener(this);
		mCacheText.setOnClickListener(this);
		mAlarmsText.setOnClickListener(this);
		mQueriesText.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.review:
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.takusemba.mapme"));
				startActivity(intent);
				break;
			case R.id.cache:
				MySnackBar.showSettingsSnackBar(this, MySnackBar.SettingAction.CACHE, mLinearLayout, mOrma);
				break;
			case R.id.alarms:
				MySnackBar.showSettingsSnackBar(this, MySnackBar.SettingAction.ALARMS, mLinearLayout, mOrma);
				break;
			case R.id.queries:
				MySnackBar.showSettingsSnackBar(this, MySnackBar.SettingAction.QUERIES, mLinearLayout, mOrma);
				break;
		}
	}
}
