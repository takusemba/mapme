package com.takusemba.mapme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.takusemba.mapme.APIs.MyGeofenceApi;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.R;
import com.takusemba.mapme.listeners.OnApiPreparedListener;
import com.takusemba.mapme.listeners.OnKeyboardClosedListener;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.AutoQuery;
import com.takusemba.mapme.models.CurrentLocation;
import com.takusemba.mapme.utils.Map;
import com.takusemba.mapme.utils.MySnackBar;
import com.takusemba.mapme.utils.OrmaUtil;
import com.takusemba.mapme.utils.Permission;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MapsActivity extends MapBaseActivity
		implements OnMapReadyCallback,
		SeekBar.OnSeekBarChangeListener,
		FloatingActionButton.OnClickListener,
		MaterialSearchView.OnQueryTextListener,
		OnKeyboardClosedListener,
		GoogleMap.OnMapClickListener {

	private boolean isMyLocationSet = false;
	private static final int ZOOM_SCALE = 12;

	private GoogleMap mMap;
	private SupportMapFragment mMapFragment;
	private Marker mMarker;
	private Circle mCircle;

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Bind(R.id.search_view)
	MaterialSearchView mSearchView;

	@Bind(R.id.fab)
	FloatingActionButton mFab;

	@Bind(R.id.seek_bar)
	SeekBar mSeekBar;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_maps);
		ButterKnife.bind(this);
		setDrawerAndAd();
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_maps);
		}
		mCoordinatorLayout = ButterKnife.findById(this, R.id.custom_coordinator_layout);
		mSearchView.setVoiceSearch(false);
		mSearchView.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
		mSearchView.setSuggestions(OrmaUtil.getSuggestions(mOrma));
		setTransition();
		mSeekBar.setOnSeekBarChangeListener(this);
		mFab.setOnClickListener(this);
		mSearchView.setOnQueryTextListener(this);
		mCoordinatorLayout.setOnKeyboardClosedListener(this);

		mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mMapFragment.getMapAsync(this);
	}

	@Override
	public void onBackPressed() {
		if (mSearchView.isSearchOpen()) {
			mSearchView.closeSearch();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		isMyLocationSet = false;
		mMap = googleMap;
		if (!Permission.isPermissionDenied(this)) {
			setMap();
		} else {
			requestPermission();
		}
		checkState();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_maps, menu);
		MenuItem item = menu.findItem(R.id.action_search);
		mSearchView.setMenuItem(item);
		return true;
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case Constants.PermissionCode.ACCESS_FINE_LOCATION:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					setMap();
				}
		}
	}

	/**
	 * Seek bar Listener
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
		if (mCircle != null) {
			mCircle.setRadius(i);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	/**
	 * FloatingActionButton Listener
	 */
	@Override
	public void onClick(View view) {
		if (MySnackBar.isShown()) {
			return;
		}
		if (mSeekBar.getProgress() == 0) {
			MySnackBar.showRadiusSnackBar(mCoordinatorLayout);
			return;
		}
		if (mMarker != null && mCircle != null) {
			double latitude = mMarker.getPosition().latitude;
			double longitude = mMarker.getPosition().longitude;
			double radius = mCircle.getRadius();
			AlarmLocation alarmLocation = new AlarmLocation(mMarker.getTitle(), latitude, longitude, (int) radius);
			AutoQuery query = new AutoQuery(alarmLocation.getName());
			mOrma.insertIntoAutoQuery(query);
			final MyGeofenceApi myGeofenceApi = new MyGeofenceApi(MapsActivity.this, latitude, longitude, radius, mOrma.insertIntoAlarmLocation(alarmLocation));
			myGeofenceApi.onStart();
			myGeofenceApi.setOnApiPreparedLister(new OnApiPreparedListener() {
				@Override
				public void ApiPrepared() {
					myGeofenceApi.registerGeofence();
					finish();
				}
			});
		} else {
			Snackbar.make(mCoordinatorLayout, R.string.snackbar_no_area, Snackbar.LENGTH_SHORT).show();
		}
	}

	/**
	 * MaterialSearchView Listener
	 */
	@Override
	public boolean onQueryTextSubmit(String query) {
		clear();
		LatLng latLng = Map.getLatLng(this, query);
		if (latLng != null) {
			mark(latLng, query);
		} else {
			Snackbar.make(mCoordinatorLayout, R.string.snackbar_no_address, Snackbar.LENGTH_SHORT).show();
			mSeekBar.setVisibility(View.GONE);
		}
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	/**
	 * CustomCoordinatorLayout Listener
	 */
	@Override
	public void keyboardClosed() {
		if (mSearchView.isSearchOpen()) {
			mSearchView.closeSearch();
		}
	}

	/**
	 * map click Listener
	 */
	@Override
	public void onMapClick(LatLng latLng) {
		clear();
		String title = Map.getTitle(this, latLng);
		mark(latLng, title);
	}

	/**
	 * Other Methods
	 */

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	private void setTransition() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mFab.setTransitionName(Constants.TransitionKey.FAB);
		}
	}

	private void setMap() {
		if (mMap == null) {
			return;
		}
		if (!OrmaUtil.isCurrentLocationEmpty(mOrma)) {
			CurrentLocation currentLocation = mOrma.selectFromCurrentLocation().get(0);
			LatLng loc = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, ZOOM_SCALE));
		}
		mMap.getUiSettings().setMapToolbarEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			mMap.setMyLocationEnabled(true);
		}
		mMap.setOnMapClickListener(this);
	}

	private void clear() {
		mMap.clear();
		mMarker = null;
		mCircle = null;
	}

	private void mark(LatLng latLng, String title) {
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_SCALE));
		mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(title));
		mCircle = mMap.addCircle(Map.getCircle(this, latLng, mSeekBar));
		mSeekBar.setVisibility(View.VISIBLE);
	}
}
