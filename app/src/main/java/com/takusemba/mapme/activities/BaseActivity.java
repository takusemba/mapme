package com.takusemba.mapme.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.takusemba.mapme.R;
import com.takusemba.mapme.adapters.NavAdapter;
import com.takusemba.mapme.models.NavItem;
import com.takusemba.mapme.models.OrmaDatabase;

/**
 * Created by takusemba on 15/11/30.
 */
public abstract class BaseActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

	private NavAdapter mNavAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	private static ActivityType whichItem;

	protected DrawerLayout mDrawerLayout;
	protected ListView mNavListView;
	protected AdView mAdView;
	protected OrmaDatabase mOrma;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mOrma = OrmaDatabase.builder(this).build();
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (getSupportActionBar() != null) {
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(true);
		}
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.string.drawer_open, R.string.drawer_close) {

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
			}

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				if (whichItem == null) {
					return;
				}
				if (!(BaseActivity.this instanceof MainActivity) && whichItem != ActivityType.MainActivity) {
					finish();
				}
				Intent intent;
				switch (whichItem) {
					case CurrentLocationActivity:
						intent = new Intent(BaseActivity.this, CurrentLocationActivity.class);
						startActivity(intent);
						break;
					case MainActivity:
						intent = new Intent(BaseActivity.this, MainActivity.class);
						startActivity(intent);
						break;
					case MapsActivity:
						intent = new Intent(BaseActivity.this, MapsActivity.class);
						startActivity(intent);
						break;
					case AlarmCheckActivity:
						intent = new Intent(BaseActivity.this, AlarmCheckActivity.class);
						startActivity(intent);
						break;
					case SettingsActivity:
						intent = new Intent(BaseActivity.this, SettingsActivity.class);
						startActivity(intent);
						break;
					default:
						break;
				}
				whichItem = null;
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerToggle.setDrawerIndicatorEnabled(true);
		mNavAdapter = new NavAdapter(this, 0, new NavItem().setNavitems(this));

		if (mNavListView.getHeaderViewsCount() == 0) {
			View DrawerHeader = getLayoutInflater().inflate(R.layout.drawer_header, null);
			mNavListView.addHeaderView(DrawerHeader, null, false);
		}
		mNavListView.setOnItemClickListener(this);
		mNavListView.setAdapter(mNavAdapter);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	protected void setDrawerAndAd() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mNavListView = (ListView) findViewById(R.id.nav_list);
		mAdView = (AdView) findViewById(R.id.ad);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				mDrawerToggle.onOptionsItemSelected(item);
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		if (adapterView.getId() == R.id.nav_list) {
			whichItem = null;
			mDrawerLayout.closeDrawers();
			switch (i) {
				//current location
				case 1:
					if (this instanceof CurrentLocationActivity) {
						return;
					}
					whichItem = ActivityType.CurrentLocationActivity;
					break;
				//alarm list
				case 2:
					if (this instanceof MainActivity) {
						return;
					}
					whichItem = ActivityType.MainActivity;
					break;
				//add alarm
				case 3:
					if (this instanceof MapsActivity) {
						return;
					}
					whichItem = ActivityType.MapsActivity;
					break;
				//check alarm
				case 4:
					if (this instanceof AlarmCheckActivity) {
						return;
					}
					whichItem = ActivityType.AlarmCheckActivity;
					break;
				case 5:
					if (this instanceof SettingsActivity) {
						return;
					}
					whichItem = ActivityType.SettingsActivity;
					break;
			}
		}
	}

	private enum ActivityType {
		CurrentLocationActivity,
		MainActivity,
		MapsActivity,
		AlarmCheckActivity,
		SettingsActivity
	}
}
