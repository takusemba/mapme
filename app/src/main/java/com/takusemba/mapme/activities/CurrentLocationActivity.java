package com.takusemba.mapme.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.takusemba.mapme.Constants;
import com.takusemba.mapme.R;
import com.takusemba.mapme.models.AlarmLocation;
import com.takusemba.mapme.models.CurrentLocation;
import com.takusemba.mapme.utils.Map;
import com.takusemba.mapme.utils.Permission;
import com.takusemba.mapme.utils.OrmaUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by takusemba on 15/11/30.
 */
public class CurrentLocationActivity extends MapBaseActivity
		implements OnMapReadyCallback,
		GoogleMap.OnMyLocationChangeListener {

	private boolean isMyLocationSet = false;
	private static final int ZOOM_SCALE = 12;

	private GoogleMap mMap;
	private SupportMapFragment mMapFragment;

	@Bind(R.id.toolbar)
	Toolbar mToolbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_current_location);
		ButterKnife.bind(this);
		setSupportActionBar(mToolbar);
		if (getSupportActionBar() != null) {
			getSupportActionBar().setTitle(R.string.title_activity_current_location);
		}
		setDrawerAndAd();
		mCoordinatorLayout = ButterKnife.findById(this, R.id.coordinator_layout);
		mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mMapFragment.getMapAsync(this);
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		isMyLocationSet = false;
		mMap = googleMap;
		if (Permission.isPermissionGranted(this)) {
			setMap();
			setAlarmLocation();
		} else {
			requestPermission();
		}
		checkState();
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		switch (requestCode) {
			case Constants.PermissionCode.ACCESS_FINE_LOCATION:
				if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					setMap();
				}
		}
	}

	/**
	 * GoogleMap Listener
	 */
	@Override
	public void onMyLocationChange(Location location) {
		if (!isMyLocationSet) {
			LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, ZOOM_SCALE));
			isMyLocationSet = true;
		}
	}

	private void setMap() {
		if (mMap == null) {
			return;
		}
		if (!OrmaUtil.isCurrentLocationEmpty(mOrma)) {
			CurrentLocation currentLocation = mOrma.selectFromCurrentLocation().get(0);
			LatLng loc = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, ZOOM_SCALE));
		}
		mMap.getUiSettings().setMapToolbarEnabled(false);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			mMap.setMyLocationEnabled(true);
		}
		mMap.setOnMyLocationChangeListener(this);
	}

	private void setAlarmLocation() {
		List<AlarmLocation> alarmLocations = mOrma.selectFromAlarmLocation().toList();
		for (AlarmLocation alarmLocation : alarmLocations) {
			if (alarmLocation.isChecked()) {
				setMarker(alarmLocation);
			}
		}
	}

	private void setMarker(AlarmLocation alarmLocation) {
		LatLng latLng = new LatLng(alarmLocation.getLatitude(), alarmLocation.getLongitude());
		mMap.addMarker(new MarkerOptions().position(latLng));
		mMap.addCircle(Map.getCircle(this, latLng, alarmLocation));
	}
}
