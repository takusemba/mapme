package com.takusemba.mapme.activities;

/**
 * Created by takusemba on 15/11/26.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;

import com.takusemba.mapme.Constants;
import com.takusemba.mapme.receivers.ConnectionReceiver;
import com.takusemba.mapme.receivers.GpsStatusReceiver;
import com.takusemba.mapme.utils.MySnackBar;
import com.takusemba.mapme.utils.Network;
import com.takusemba.mapme.utils.Permission;
import com.takusemba.mapme.widgets.CustomCoordinatorLayout;

public abstract class MapBaseActivity extends BaseActivity implements ConnectionReceiver.Observer, GpsStatusReceiver.Observer {

	protected CustomCoordinatorLayout mCoordinatorLayout;

	private boolean isRegistered = false;
	private ConnectionReceiver mConnectionReceiver;
	private static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
	private GpsStatusReceiver mGpsStatusReceiver;
	private static final String GPS_STATUS_CHANGE = "android.location.GPS_ENABLED_CHANGE";

	private boolean isPermissionDialogShown = false;

	@Override
	protected void onResume() {
		super.onResume();
		setConnectionReceiver();
		setGpsStatusReceiver();
		isRegistered = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (isRegistered) {
			unregisterReceiver(mConnectionReceiver);
			unregisterReceiver(mGpsStatusReceiver);
		}
		if (MySnackBar.isShown()) {
			MySnackBar.dismiss();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case Constants.PermissionCode.ACCESS_FINE_LOCATION:
				isPermissionDialogShown = false;
				checkPermissionState(mCoordinatorLayout);
		}
	}

	public void onConnectionStatusChange() {
		checkNetworkState(mCoordinatorLayout);
	}

	public void onGpsStatusChange() {
		checkCurrentLocationState(mCoordinatorLayout);
	}

	protected void checkState() {
		checkNetworkState(mCoordinatorLayout);
		checkPermissionState(mCoordinatorLayout);
		checkCurrentLocationState(mCoordinatorLayout);
	}

	public void checkNetworkState(CoordinatorLayout coordinatorLayout) {
		if (!Network.isConnected(this)) {
			MySnackBar.showNetworkSnackBar(coordinatorLayout);
		} else {
			MySnackBar.dismissNetworkSnackBar();
		}
	}

	private void checkPermissionState(CoordinatorLayout coordinatorLayout) {
		if (!isPermissionDialogShown && Permission.isPermissionDenied(this)) {
			MySnackBar.showPermissionSnackBar(this, coordinatorLayout);
		} else {
			MySnackBar.dismissPermissionSnackBar();
		}
	}

	public void setPermissionDialogShown(boolean isShown) {
		isPermissionDialogShown = isShown;
	}

	private void checkCurrentLocationState(CoordinatorLayout coordinatorLayout) {
		if (Permission.isCurrentLocationDenied(this)) {
			MySnackBar.showCurrentLocationSnackBar(this, coordinatorLayout);
		} else {
			MySnackBar.dismissCurrentLocationSnackBar();
		}
	}

	protected void requestPermission() {
		isPermissionDialogShown = true;
		Permission.requestPermission(this, 0, Manifest.permission.ACCESS_FINE_LOCATION);
	}

	private void setConnectionReceiver() {
		IntentFilter filter = new IntentFilter(CONNECTIVITY_CHANGE);
		mConnectionReceiver = new ConnectionReceiver(MapBaseActivity.this);
		registerReceiver(mConnectionReceiver, filter);
	}

	private void setGpsStatusReceiver() {
		IntentFilter filter = new IntentFilter(GPS_STATUS_CHANGE);
		mGpsStatusReceiver = new GpsStatusReceiver(MapBaseActivity.this);
		registerReceiver(mGpsStatusReceiver, filter);
	}
}
